<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fitnescoaching
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
if(is_front_page()){
	$class_menu='transparent';
}else{
	$class_menu='red';
}
?>
<div id="page" class="site">
	<header id="masthead" class="<?php echo $class_menu; ?> site-header" role="banner">
		<div class="small-12 medium-6 large-6 columns site-branding">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/logofitnes.png"></a>
		</div><!-- .site-branding -->
		<nav id="site-navigation" class="small-12 medium-6 large-6 columns main-navigation" role="navigation">
			<a class="menu-button"></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<a target="_blank" href="https://www.facebook.com/fitnessmid/?fref=ts"><div class="glyph-icon flaticon-socialnetwork292"></div></a>
	<a target="_blank" href="#"><div class="glyph-icon flaticon-twitter25"></div></a>

	<div id="content" class="large-12 columns site-content">
