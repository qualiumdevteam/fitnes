jQuery(document).ready(function() {
    var ancho = jQuery('body').width();
    var cuartotamano = jQuery('body').height()/4;
    var alto = cuartotamano*3;
    jQuery('.triangle').css('border-left',''+ancho+'px solid transparent');
    jQuery('.triangle').css('border-bottom', 'solid '+alto+'px #DC2A2A');
    jQuery('.triangle2').css('border-top',''+alto+'px solid #DC2A2A');
    jQuery('.triangle2').css('border-left', 'solid '+ancho+'px transparent');
    jQuery('.triangulo1').css('border-top', '' + cuartotamano*2 + 'px solid #F8F8F8');
    jQuery('.triangulo1').css('border-right', 'solid ' + ancho + 'px transparent');
    jQuery('.triangulo2').css('border-bottom', '' + cuartotamano*2 + 'px solid #ECEAE5');
    jQuery('.triangulo2').css('border-left', 'solid ' + ancho + 'px transparent');
    jQuery('.triangulocontacto').css('border-bottom', 'solid ' + alto/4 + 'px #DC2A2A');
    jQuery('.triangulocontacto').css('border-right', 'solid ' + ancho/2 + 'px transparent');
    jQuery('.trianguloblog').css('border-bottom', 'solid 150px #F8F8F8');
    jQuery('.trianguloblog').css('border-left', 'solid ' + ancho + 'px transparent');
    jQuery('.trianguloblog2').css('border-bottom', 'solid 100px #0D0F13');
    jQuery('.trianguloblog2').css('border-left', 'solid ' + ancho + 'px transparent');
    jQuery('.tringulo_contactohome').css('border-right', 'solid ' + ancho + 'px transparent');
    jQuery('.triangulocontactotop').css({'border-top':'400px solid #ECEAE5','border-left': ''+ancho+'px solid transparent'});

    if(ancho < 1024){
        jQuery('#contentrelacionado').addClass('contenedor_relacionado');
        jQuery('.tringulo_contactohome').show();
        jQuery('.triangulofootertextotop').css('border-left', 'solid ' + ancho + 'px #DC2A2A');
    }
    else{
        jQuery('#contentrelacionado').removeClass('contenedor_relacionado');
        jQuery('.tringulo_contactohome').hide();
    }

    jQuery('.metaarticulo').on('click','a',function(){
        return false;
    })

    if(!jQuery('body').hasClass('home')){
        jQuery('#menu-item-270').click(function(){
            window.location=location.origin+'/#contacto';
        })

        jQuery('#menu-item-270').click(function(){
            window.location=location.origin+'/#contacto';
        })
    }

    if(window.location.hash) {
        var x = location.hash;
        var target = jQuery(x);
        if (target.length) {
            position=target.offset().top;
            jQuery('html,body').animate({
                scrollTop: position
            }, 1000);
        }
    } else {

    }

    jQuery(window).resize(function(){
        var ancho = jQuery('body').width();
        var windowsalto = jQuery('body').height();

        if(ancho< 1024){
            jQuery('#contentrelacionado').addClass('contenedor_relacionado');
            jQuery('.tringulo_contactohome').show();
            jQuery('.triangulofootertextotop').css('border-left', 'solid ' + ancho + 'px #DC2A2A');
        }
        else{
            jQuery('#contentrelacionado').removeClass('contenedor_relacionado');
            jQuery('.tringulo_contactohome').hide();
        }

        if(windowsalto>700) {
            var cuartotamano = jQuery('body').height() / 4;
            var alto = cuartotamano * 3;
            jQuery('.triangle').css('border-left', '' + ancho + 'px solid transparent');
            jQuery('.triangle').css('border-bottom', 'solid ' + alto + 'px #DC2A2A');
            jQuery('.triangle2').css('border-top', '' + alto + 'px solid #DC2A2A');
            jQuery('.triangle2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulo1').css('border-top', '' + cuartotamano*2 + 'px solid #F8F8F8');
            jQuery('.triangulo1').css('border-right', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulo2').css('border-bottom', '' + cuartotamano*2 + 'px solid #ECEAE5');
            jQuery('.triangulo2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulocontacto').css('border-bottom', 'solid ' + alto/4 + 'px #DC2A2A');
            jQuery('.triangulocontacto').css('border-right', 'solid ' + ancho/2 + 'px transparent');
            jQuery('.trianguloblog').css('border-bottom', 'solid 150px #F8F8F8');
            jQuery('.trianguloblog').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.trianguloblog2').css('border-bottom', 'solid 100px #0D0F13');
            jQuery('.trianguloblog2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulocontactotop').css({'border-top':'400px solid #ECEAE5','border-left': ''+ancho+'px solid transparent'});
        }
        else {
            var cuartotamano = jQuery('body').height();
            var alto = cuartotamano * 1;
            jQuery('.triangle').css('border-left', '' + ancho + 'px solid transparent');
            jQuery('.triangle').css('border-bottom', 'solid ' + alto + 'px #DC2A2A');
            jQuery('.triangle2').css('border-top', '' + alto + 'px solid #DC2A2A');
            jQuery('.triangle2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulo1').css('border-top', '' + cuartotamano*2 + 'px solid #F8F8F8');
            jQuery('.triangulo1').css('border-right', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulo2').css('border-bottom', '' + cuartotamano*1 + 'px solid #ECEAE5');
            jQuery('.triangulo2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulocontacto').css('border-bottom', 'solid ' + alto/4 + 'px #DC2A2A');
            jQuery('.triangulocontacto').css('border-right', 'solid ' + ancho/2 + 'px transparent');
            jQuery('.trianguloblog').css('border-bottom', 'solid 150px #F8F8F8');
            jQuery('.trianguloblog').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.trianguloblog2').css('border-bottom', 'solid 100px #0D0F13');
            jQuery('.trianguloblog2').css('border-left', 'solid ' + ancho + 'px transparent');
            jQuery('.triangulocontactotop').css({'border-top':'400px solid #ECEAE5','border-left': ''+ancho+'px solid transparent'});
        }
    })

    jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                position=target.offset().top-35;
                jQuery('html,body').animate({
                    scrollTop: position
                }, 1000);
                return false;
            }
        }
    });

    if(jQuery('body').hasClass('home') && ancho > 640) {
        var win = jQuery(window);
        var pos = jQuery('#textobottom').position();
        var pos2 = jQuery('.descripcion').position();
        console.log(pos2);
        win.scroll(function () {
            if (win.scrollTop() >= pos.top) {
                jQuery('.imgbottom').addClass('mostrar');
            }
            if (win.scrollTop() >= pos2.top) {
                jQuery('.pesa1').addClass('mostrar');
                jQuery('.pesa3').addClass('mostrar');

            }
        });
    }

    jQuery('.contenedor_relacionado').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        navText:['Anterior','Siguiente'],
        dots: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    /*jQuery('.sendform').click(function(){
        var nombre=jQuery('.nombre').val();
        var email=jQuery('.email').val();
        var telefono=jQuery('.telefonoform').val();
        var meta=jQuery('.meta').val();
        var forma_contacto=jQuery('.forma_contacto').val();
        var mensaje=jQuery('.mensaje_form').val();

        if(nombre && email && telefono && meta && forma_contacto!=''){
            jQuery.ajax({
                url: 'wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    action: 'sendform',
                    nombre: nombre,
                    email: email,
                    telefono: telefono,
                    meta: meta,
                    formacontacto: forma_contacto,
                    mensaje: mensaje
                },
                beforeSend: function( xhr ) {
                    jQuery('.response').show();
                },
                success: function(data) {
                    jQuery('.response').hide();
                    jQuery('.nombre').val('');
                    jQuery('.email').val('');
                    jQuery('.telefonoform').val('');
                    jQuery('.meta').val('');
                    jQuery('.forma_contacto').val('');
                    jQuery('.mensaje_form').val('');
                    if(data==1){
                        jQuery('.mensajeenvio').text('Mensaje enviado correctamente');
                    }
                    else{
                        jQuery('.mensajeenvio').text('Mensaje No enviado intente de nuevo');
                    }
                },
                error: function() {
                    jQuery('.nombre').val('');
                    jQuery('.email').val('');
                    jQuery('.telefonoform').val('');
                    jQuery('.meta').val('');
                    jQuery('.forma_contacto').val('');
                    jQuery('.mensaje_form').val('');
                    jQuery('.mensajeenvio').text('Mensaje No enviado intente de nuevo');
                },
            });
        }else{
            jQuery('.mensajeenvio').text('Campos incompletos');
        }
    })*/

    jQuery('.menu-button').click(function(){
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('mostrar');
        jQuery('.overlay_footer').toggleClass('activo');
    })

});