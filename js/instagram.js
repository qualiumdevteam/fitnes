var access_token = "16384709.6ac06b4.49b97800d7fd4ac799a2c889f50f2587",
    access_parameters = {
        access_token: access_token
    };

var form = jQuery('#tagsearch');
form.on('submit', function(ev) {
    var q = this.tag.value;
    if(q.length) {
        //console.log(q);
        grabImages(q, 4, access_parameters);
    }
    ev.preventDefault();
});

function grabImages(tag, count, access_parameters) {
    var instagramUrl = 'https://api.instagram.com/v1/tags/' + tag + '/media/recent?callback=?&count=' + count;
    jQuery.getJSON(instagramUrl, access_parameters, onDataLoaded);
}

function onDataLoaded(instagram_data) {
    var target = jQuery(".testi");
    var ancho = jQuery('body').width();
    //console.log(instagram_data);
    if (instagram_data.meta.code == 200) {
        var photos = instagram_data.data;
        console.log(photos);
        if (photos.length > 0) {
            target.empty();
            if(ancho>800) {
                for (var key in photos) {
                    var photo = photos[key];
                    //target.append('<a href="' + photo.link + '"><img src="' + photo.images.standard_resolution.url + '"></a>')
                    target.append('<div class="item_testi"><div style="background-image: url(' + photo.images.standard_resolution.url + ')" class="large-4 columns img"></div><div class="large-8 columns texto"><p>' + photo.caption.text + '</p></div></div>');
                }
            }else{
                for (var key in photos) {
                    var photo = photos[key];
                    var texto=photo.caption.text;
                    //target.append('<a href="' + photo.link + '"><img src="' + photo.images.standard_resolution.url + '"></a>')
                    target.append('<div class="item_testi"><div style="background-image: url(' + photo.images.standard_resolution.url + ')" class="large-4 columns img"></div><div class="large-8 columns texto"><p>' + texto.substring(0,250) + '...</p></div><div class="clearfix"></div><a class="moretesti" target="_blank" href="'+photo.link+'">Ver mas</a></div>');
                }
            }

            jQuery('.testi').owlCarousel({
                loop:false,
                margin:10,
                nav:false,
                navText:['Anterior','Siguiente'],
                dots: true,
                responsive:{
                    0:{
                        items:1
                    },
                    800:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1024:{
                        items:1
                    },
                    1000:{
                        items:2
                    }
                }
            })
        } else {
            target.html("nothing found");
        }
    } else {
        var error = instagram_data.meta.error_message;
        target.html(error);
    }
}

jQuery(window).resize(function(){
    jQuery('.testi').trigger('destroy.owl.carousel');
    grabImages('Soyfitnesscoaching', 4, access_parameters);
});

grabImages('Soyfitnesscoaching', 4, access_parameters);