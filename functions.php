<?php
/**
 * fitnescoaching functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package fitnescoaching
 */

if ( ! function_exists( 'fitnescoaching_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function fitnescoaching_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on fitnescoaching, use a find and replace
	 * to change 'fitnescoaching' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'fitnescoaching', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'fitnescoaching' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'fitnescoaching_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'fitnescoaching_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function fitnescoaching_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'fitnescoaching_content_width', 640 );
}
add_action( 'after_setup_theme', 'fitnescoaching_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function fitnescoaching_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'fitnescoaching' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'fitnescoaching_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function fitnescoaching_scripts() {
	wp_enqueue_style( 'fitnescoaching-style', get_stylesheet_uri() );
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.min.css');
	wp_enqueue_style( 'carosel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '20120206', false );
	wp_enqueue_style( 'theme-carosel', get_template_directory_uri() . '/css/owl.theme.default.css', array(), '20120206', false );
	wp_enqueue_style( 'icon', get_template_directory_uri() . '/icon/flaticon.css', array(), '20120206', false );
	wp_enqueue_style( 'loader', get_template_directory_uri() . '/css/loader.css', array(), '20120206', false );

	wp_enqueue_script( 'fitnescoaching-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'fitnescoaching-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20120206', true );
	wp_enqueue_script( 'hacienda-teya-owlcarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20120206', true );
	wp_enqueue_script( 'fitnescoaching-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'fitnescoaching-instagram', get_template_directory_uri() . '/js/instagram.js', array(), '20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'fitnescoaching_scripts' );

add_action('admin_print_scripts', 'wp_fancy_commerce_admin_scripts');
function wp_fancy_commerce_admin_scripts(){
	wp_enqueue_script( 'fitnescoaching-mainadmin', get_template_directory_uri() . '/js/main_admin.js', array(), '20120206', true );
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_script('common');
	wp_enqueue_script('jquery');
	wp_enqueue_style( 'adminstyle', get_template_directory_uri() . '/css/admin_style.css', array(), '20120206', false );
}

function get_pagination($query){
	$big = 999999999; // need an unlikely integer
	return paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		//'base' => @add_query_arg('page','%#%'),
		'format' => '?paged=%#%',
		'prev_text'    => __('&laquo;'),
		'current' => max(1,get_query_var('paged') ),
		'next_text'    => __('&raquo;'),
		'total' => $query->max_num_pages,
	) );
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

require get_template_directory() . '/inc/metaboxes.php';


/*add_action( 'wp_ajax_sendform', 'sendform' );
add_action( 'wp_ajax_nopriv_sendform', 'sendform' );

function sendform(){

	add_filter( 'wp_mail_from', 'my_mail_from' );
	function my_mail_from( $email )
	{
		return "noreply@fitnesscoaching.mx";
	}

	add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
	function my_mail_from_name( $name )
	{
		return "fitness Coaching";
	}

	//$to='festrada@fitnesscoaching.mx';
	$to='festrada@fitnesscoaching.mx';
	$subject2 ='Nuevo mensaje desde fitnesscoaching.mx';
	$headers[] = 'Reply-to: noreply@eternitta.com.mx\' . "\r\n"';
	//$headers[] = 'BCC: direccion@qualium.mx';
	$message2 ='<p>Nombre: '.$_POST['nombre'].'</p>';
	$message2 .='<p>E-mail: '.$_POST['email'].'</p>';
	$message2 .='<p>Teléfono: '.$_POST['telefono'].'</p>';
	$message2 .='<p>¿Cual es tu mayor reto?: '.$_POST['meta'].'</p>';
	$message2 .='<p>¿Como te gustaría que nos contactemos?: '.$_POST['formacontacto'].'</p>';
	$message2 .='<p>Mensaje: '.$_POST['mensaje'].'</p>';
	add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

	if(wp_mail( $to, $subject2, $message2, $headers)){
		echo 1;
	}else{
		echo 2;
	}

	exit();
}*/


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
