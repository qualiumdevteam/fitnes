<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fitnescoaching
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="large-12 columns site-footer" role="contentinfo">
		<div class="large-6 columns text-left politicas">
			<a href="">Políticas de privacidad</a>
		</div>
		<div class="large-6 columns text-right copy">
			<p>Todos los derechos reservados – <?php echo date('Y'); ?></p>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="overlay_footer"></div>
<?php wp_footer(); ?>

</body>
</html>
