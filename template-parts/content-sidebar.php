<?php
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'paged' => $paged,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>
<div id="contentrelacionado">
<?php
if($query->have_posts()){
while($query->have_posts()) : $query->the_post();
$feat_image_sidebar = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
?>
    <div class="articulo_relacionado">
        <a href="<?php echo get_the_permalink(); ?>"><div style="background-image: url('<?php echo $feat_image_sidebar; ?>')" class="imgdestacada"></div></a>
        <p class="tituloarticulo text-right"><?php echo get_the_title(); ?></p>
        <p class="contenidoarticulorelacionado"><?php echo substr(strip_tags(get_the_content()),0,100); ?></p>
    </div>
<?php  endwhile; }  ?>
</div>
