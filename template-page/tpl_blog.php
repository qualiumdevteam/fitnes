<?php
/*
* Template Name: Blog
*/
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
    'paged' => $paged,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>
<section class="blog">
    <div class="banner" style="background-image: url('<?php echo $feat_image ?>')">
        <div class="formsuscribe">
            <h1 class="text-center titulo">SÉ PARTE DEL <span>EQUIPO FITNESS</span></h1>
            <p class="describe">Únete a nuestra comunidad y recibe consejos gratis sobre fitness y nutricion</p>
            <!-- BEGIN: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>

            <style type="text/css">
                .bmform_outer646535{} .bmform_inner646535{width:100%;} .bmform_head646535{background-color:transparent; display: none height:37px;} .bm_headetext646535{color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:18px; padding:6px 10px 0 10px; font-weight:bold;} .bmform_body646535{background-color:transparent; color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:12px; padding:12px;} .bmform_introtxt646535{font-family:Arial, Helvetica, sans-serif; font-size:12px; padding-bottom:12px;} .bmform_frmtext646535{padding: 5px 0px 3px 0px; display:block; float:none; text-align: left; text-decoration: none; width: auto; font-weight:bold;} .bmform_frm646535{color:#000000; display:block; float:none; font-family:Verdana,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-align:left; text-decoration:none; padding:3px;} .bmform_button646535{text-align:center; padding-top:15px;} .bmform_submit646535{} .bmform_footer646535{}.footer_bdy646535{}.footer_txt646535{}#tdLogo646535 img{margin-bottom:10px; max-width:230px;}</style>
            <script type="text/javascript">
                function CheckField646535(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
                function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
                function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
                function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
                function CheckFieldD646535(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate646535(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {	nDate = ""; retVal = false;	} else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
                function validDate646535(chkDD, chkMM, chkYY, frm) {var objDate = null;	if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
                function _checkSubmit646535(frm){
                    if ( !isemail(frm["fldEmail"].value) ) {
                        alert("Por favor introduzca el Email");
                        return false;
                    }
                    return true; }
            </script>
            <div align="center">
                <form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB646535" accept-charset="UTF-8" onsubmit="return _checkSubmit646535(this);" >
                    <input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
                    <input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
                    <input type=hidden name=token value="mFcQnoBFKMQZ5X5VKB2zNeZXUawFafLCyKDIlnVHWkqZ5kqONGyEzw%3D%3D" />
                    <input type=hidden name=doubleoptin value="" />
                    <div class=bmform_outer646535 id=tblFormData646535>
                        <div class=bmform_inner646535>
                            <div class=bmform_head646535 id=tdHeader646535>
                                <div class=bm_headetext646535></div></div>
                            <div class=bmform_body646535>
                                <div class=bmform_introtxt646535 id=tdIntro646535 >

                                </div>
                                <div id=tblFieldData646535 style='text-align:left;'>

                                    <div class=bmform_frmtext646535>
                                            </div>
                                    <input type=text class=bmform_frm646535 placeholder="Correo electrónico" name=fldEmail maxlength=100 />
                                </div>

                                <div class=bmform_button646535><input type="submit" id="btnSubmit" value="Suscribir"  krydebug="1751" class=bmform_submit646535 />
                                </div></div>
                            <div class=bmform_footer646535><div class=footer_bdy646535><div class=footer_txt646535></div></div></div>
                        </div></div>
                    <table id="tblRequiredField646535" width="220" border="0" cellspacing="0" cellpadding="0" ><tr><td align=right style='display:none; font-size:10px;'>

                            </td></tr></table>
                </form></div>
            <!-- BEGIN: Email Marketing By Benchmark Email ------><div align="center" style="padding-top:5px;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"><a href="//www.benchmarkemail.com" target=_new style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"></a></div><!-- END: Email Marketing By Benchmark Email ---------->

            <!-- END: Signup Form Manual Code from Benchmark Email Ver 2.0 ------>
        </div>
        <div class="trianguloblog"></div>
    </div>
    <div class="listadoarticulos">
        <?php
        if($query->have_posts()){
        while($query->have_posts()) : $query->the_post();
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
        ?>
        <div class="item small-12 medium-6 large-4 columns">
            <a href="<?php echo get_the_permalink(); ?>"><div style="background-image: url('<?php echo $feat_image; ?>')" class="portada_articulo"></div></a>
            <a href="<?php echo get_the_permalink(); ?>"><p class="titulo_articulo"><?php echo get_the_title(); ?></p></a>
            <p class="metaarticulo"><?php echo get_the_date(); ?></p>
            <p class="metaarticulo">por: <?php the_author_posts_link() ?></p>
            <div class="clearfix"></div>
            <p class="contenidoarticulo"><?php echo substr(strip_tags(get_the_content()),0,200); ?></p>
        </div>
        <?php  endwhile; }  ?>
        <div class="clearfix"></div>
        <div class="paginacion text-center">Pag.<?php echo get_pagination($query) ?></div>
        <div class="trianguloblog2">
    </div>
</section>
<?php get_footer(); ?>
