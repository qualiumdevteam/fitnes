<?php
/*
* Template Name: Home
*/
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
$sec2 = get_post_meta($post->ID,'sec2',true);
$sec3 = get_post_meta($post->ID,'sec3',true);
$sec4 = get_post_meta($post->ID,'sec4',true);
$sec5 = get_post_meta($post->ID,'sec5',true);
$sec6 = get_post_meta($post->ID,'sec6',true);
$faq = get_post_meta($post->ID,'faq',true);
?>
<section style="background-image: url('<?php echo $feat_image ?>')" class="portada">
    <div class="triangle"></div>
    <div class="contenido">
    <h1 class="titulo">EL MEJOR CUERPO DE TU VIDA
        TE ESTÁ ESPERANDO</h1>
        <p class="hash">ÚNETE AL <span>#3mesesfit</span></p>
        <div class="texto">
            <?php while(have_posts()) : the_post(); ?>
                <?php echo strip_tags(get_the_content()); ?>
            <?php endwhile; ?>
        </div>
    </div>
    <a class="down" href="#2"><img src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
</section>
<section id="2" class="descripcion">
    <div class="triangle2"></div>
    <div class="contenido_descripcion">
        <div class="infotop">
            <h1 class="titulo">cambiamos cuerpos, convertimos mentes</h1>
            <h1 class="titulo sub">transformamos vidas</h1>
            <div class="border_responsivo"></div>
            <div class="clearfix"></div>
            <p><?php echo $sec2['textotop']; ?></p>
            <div class="triangulofootertextotop"></div>
        </div>
        <div class="clearfix"></div>
        <div class="infobottom">
            <img class="imgbottom" src="<?php echo json_decode($sec2['imgtextbottom'])[0]->full ?>">
            <p id="textobottom"><?php echo $sec2['textobottom']; ?></p>
        </div>
    </div>
    <a class="down" href="#3"><img src="<?php echo get_template_directory_uri() ?>/img/down2.png"></a>
</section>
<section id="3" class="pesas">
    <img class="pesa1" src="<?php echo get_template_directory_uri() ?>/img/Pesa3-01.png">
    <img class="pesa2" src="<?php echo get_template_directory_uri() ?>/img/Pesa4-01.png">
    <img class="pesa3" src="<?php echo get_template_directory_uri() ?>/img/pesa1.png">
    <img class="pesa4" src="<?php echo get_template_directory_uri() ?>/img/pesa1.png">
</section>
<section class="programa">
    <div style="background-image: url('<?php echo json_decode($sec3['imgbanner'])[0]->full ?>')" class="banner">
        <div class="center">
            <h1 class="titulo">únete a nuestro programa</h1>
            <!--<div class="large-6 columns text-center"><h1 class="cien">#3mesesfit</h1></div>-->
            <div class="large-12 columns text-right leyendas"><p class="hash">#3mesesfit</p></div>
        </div>
    </div>
    <div class="cinta"><h3>¿EN QUÉ CONSISTE EL PROGRAMA FITNESS COACHING?</h3></div>
    <div class="small-12 medium-4 large-4 column caja1 text-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/manzana.png">
        <h3>Plan nutricional</h3>
        <p><?php echo $sec3['caja1']; ?></p>
    </div>
    <div class="small-12 medium-4 large-4 column caja2 text-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/personapesas.png">
        <h3>Rutina de ejercicios</h3>
        <p><?php echo $sec3['caja2']; ?></p>
    </div>
    <div class="small-12 medium-4 large-4 column caja3 text-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/reloj.png">
        <h3>Seguimientos</h3>
        <p><?php echo $sec3['caja3']; ?></p>
    </div>
</section>
<section class="video">
    <div class="content">
        <h1 class="titulo text-center">Recibe apoyo por parte del coach en motivación</h1>
        <?php echo $sec4; ?>
    </div>
</section>
<section style="background-image: url('<?php echo json_decode($sec5)[0]->full ?>')" class="atencion">
    <div class="overlay"></div>
    <div class="triangulo1"></div>
    <div class="triangulo2"></div>
    <h1 class="titulo">¡atendemos a toda la república!</h1>
</section>
<section class="faq">
    <h1 class="titulo">Faq</h1>
    <div><?php echo nl2br($faq) ?></div>
</section>
<section class="testimonios">
    <h1 class="titulo">Testimonios</h1>
    <div class="testi"></div>
</section>
<section class="pagos">
    <h1 class="titulo">¿qué esperas para cambiar tu vida?</h1>
    <p class="inversion">Inversión total en el programa: <span class="costo">$5,220</span></p>
    <div class="medium-6 large-6 columns opcionpago text-right">
        <form name="_xclick" action="https://www.paypal.com/mx/cgi-bin/webscr" method="post" target="_blank">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="soporte@fitnesscoaching.mx">
            <input type="hidden" class="currency_code" name="currency_code" value="MXN">
            <input id="nombre_item" type="hidden" name="item_name" value="Paquete FitnessCoaching">
            <input type="hidden" name="amount" value="5220">
            <input type="hidden" name="item_number" value="">
            <input type="submit" border="0" name="submit" value="Tarjeta de credito" alt="PayPal, la forma más segura y rápida de pagar en línea.">
        </form>
    </div>
    <div class="medium-6 large-6 columns opcionpago text-left">
        <form action="https://compropago.com/comprobante" method="post" target="_blank">
            <input type="hidden" name="public_key" value="pk_live_95727d22e934461a6">
            <input type="hidden" class="total_res" name="product_price" value="5220">
            <input type="hidden" name="product_name" value="Paquete FitnessCoaching">
            <input type="hidden" name="product_id" value="">
            <input type="hidden" name="customer_name" value="">
            <input type="hidden" name="customer_email" value="">
            <input type="hidden" name="customer_phone" value="">
            <input type="hidden" name="image_url" value="">
            <input type="hidden" name="success_url" value="">
            <input type="hidden" name="failed_url" value="">
            <input class="boton_pagos" type="submit" border="0" name="submit" value="Efectivo" alt="Pagar con ComproPago">
        </form>
    </div>
</section>
<section id="contacto" class="contacto">
    <div class="triangulocontactotop"></div>
    <div class="contenido">
        <h1 class="titulo text-center">¿AÚN TIENES DUDAS?</h1>
        <div class="text-center agenda"><p class="cirulo1"></p><p class="subtitulo text-center">agenda una cita para un plan de ejercicios diseñado a la medida</p><p class="cirulo1"></p></div>
        <div class="large-6 columns datos_contacto">
            <div class="tringulo_contactohome"></div>
            <div class="content">
                <?php echo nl2br($sec6) ?>
                <br><br>
                TELÉFONO<br>
                <img class="logowhatapp" src="<?php echo get_template_directory_uri() ?>/img/whatapp.svg"> (999)3503915
            </div>
        </div>
        <div class="large-6 columns form_contacto">
            <?php //echo do_shortcode('[contact-form-7 id="18" title="Contacto"]') ?>
          <!--  form  -->
            <script type="text/javascript">
            function CheckField661886(fldName, frm){ if ( frm[fldName].length ) { for ( var i = 0, l = frm[fldName].length; i < l; i++ ) {  if ( frm[fldName].type =='select-one' ) { if( frm[fldName][i].selected && i==0 && frm[fldName][i].value == '' ) { return false; }  if ( frm[fldName][i].selected ) { return true; } }  else { if ( frm[fldName][i].checked ) { return true; } }; } return false; } else { if ( frm[fldName].type == "checkbox" ) { return ( frm[fldName].checked ); } else if ( frm[fldName].type == "radio" ) { return ( frm[fldName].checked ); } else { frm[fldName].focus(); return (frm[fldName].value.length > 0); }} }
            function rmspaces(x) {var leftx = 0;var rightx = x.length -1;while ( x.charAt(leftx) == ' ') { leftx++; }while ( x.charAt(rightx) == ' ') { --rightx; }var q = x.substr(leftx,rightx-leftx + 1);if ( (leftx == x.length) && (rightx == -1) ) { q =''; } return(q); }
            function checkfield(data) {if (rmspaces(data) == ""){return false;}else {return true;}}
            function isemail(data) {var flag = false;if (  data.indexOf("@",0)  == -1 || data.indexOf("\\",0)  != -1 ||data.indexOf("/",0)  != -1 ||!checkfield(data) ||  data.indexOf(".",0)  == -1  ||  data.indexOf("@")  == 0 ||data.lastIndexOf(".") < data.lastIndexOf("@") ||data.lastIndexOf(".") == (data.length - 1)   ||data.lastIndexOf("@") !=   data.indexOf("@") ||data.indexOf(",",0)  != -1 ||data.indexOf(":",0)  != -1 ||data.indexOf(";",0)  != -1  ) {return flag;} else {var temp = rmspaces(data);if (temp.indexOf(' ',0) != -1) { flag = true; }var d3 = temp.lastIndexOf('.') + 4;var d4 = temp.substring(0,d3);var e2 = temp.length  -  temp.lastIndexOf('.')  - 1;var i1 = temp.indexOf('@');if (  (temp.charAt(i1+1) == '.') || ( e2 < 1 ) ) { flag = true; }return !flag;}}
            function CheckFieldD661886(fldH, chkDD, chkMM, chkYY, reqd, frm){ var retVal = true; var dt = validDate661886(chkDD, chkMM, chkYY, frm); var nDate = frm[chkMM].value  + " " + frm[chkDD].value + " " + frm[chkYY].value; if ( dt == null && reqd == 1 ) {  nDate = ""; retVal = false; } else if ( (frm[chkDD].value != "" || frm[chkMM].value != "" || frm[chkYY].value != "") && dt == null) { retVal = false; nDate = "";} if ( retVal ) {frm[fldH].value = nDate;} return retVal; }
            function validDate661886(chkDD, chkMM, chkYY, frm) {var objDate = null; if ( frm[chkDD].value != "" && frm[chkMM].value != "" && frm[chkYY].value != "" ) {var mSeconds = (new Date(frm[chkYY].value - 0, frm[chkMM].selectedIndex - 1, frm[chkDD].value - 0)).getTime();var objDate = new Date();objDate.setTime(mSeconds);if (objDate.getFullYear() != frm[chkYY].value - 0 || objDate.getMonth()  != frm[chkMM].selectedIndex - 1  || objDate.getDate() != frm[chkDD].value - 0){objDate = null;}}return objDate;}
            function _checkSubmit661886(frm){
            if ( !CheckField661886("fldfirstname", frm) ) { 
               alert("Por favor introduzca el Nombre");
               return false;
            }
            if ( !isemail(frm["fldEmail"].value) ) { 
               alert("Por favor introduzca el Email");
               return false;
            }
            if ( !CheckField661886("fldfield6", frm) ) { 
               alert("Por favor introduzca el Tel\u00E9fono");
               return false;
            }
            if ( !CheckField661886("fldfield18", frm) ) { 
               alert("Por favor introduzca el Mensaje");
               return false;
            }
            if ( !CheckField661886("fldfield21", frm) ) { 
               alert("Por favor Ingrese \u00BFCual es tu mayor reto?");
               return false;
            }
            if ( !CheckField661886("fldfield22", frm) ) { 
               alert("Por favor Ingrese \u00BFComo te gustar\u00EDa que nos contactemos?");
               return false;
            }
             return true; }
            </script>
            <div align="center">
            <form style="display:inline;" action="https://lb.benchmarkemail.com//code/lbform" method=post name="frmLB661886" accept-charset="UTF-8" onsubmit="return _checkSubmit661886(this);" >
            <input type=hidden name=successurl value="http://www.benchmarkemail.com/Code/ThankYouOptin?language=spanish" />
            <input type=hidden name=errorurl value="http://lb.benchmarkemail.com//Code/Error" />
            <input type=hidden name=token value="mFcQnoBFKMQrxam88L1%2BR%2BZXUawFafLCyKDIlnVHWkqZ5kqONGyEzw%3D%3D" />
            <input type=hidden name=doubleoptin value="" />
            <div class=bmform_outer661886 id=tblFormData661886>
            <div class=bmform_inner661886>
            <div class=bmform_head661886 id=tdHeader661886>
            <div class=bm_headetext661886></div></div>
            <div class=bmform_body661886>
            <div class=bmform_introtxt661886 id=tdIntro661886 >

            </div>
            <div id=tblFieldData661886 style='text-align:left;'>

            <div class=bmform_frmtext661886>
            Nombre <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <input type=text class=bmform_frm661886 name=fldfirstname maxlength=100 />

            <div class=bmform_frmtext661886>
            Email <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <input type=text class=bmform_frm661886 name=fldEmail maxlength=100 />

            <div class=bmform_frmtext661886>
            Tel&#233;fono <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <input type=text class=bmform_frm661886 name=fldfield6 maxlength=100 />


            <div class=bmform_frmtext661886>
            &#191;Cual es tu mayor reto? <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <select style='width:100%' name=fldfield21>
            <option value="Seguir Un Programa de Alimentacion" >Seguir Un Programa de Alimentacion</option>
            <option value="Seguir Una Rutina De Ejercicios" >Seguir Una Rutina De Ejercicios</option>
            <option value="Aprender A Comer" >Aprender A Comer</option>
            <option value="Competir En Alg&#250;n Deporte" >Competir En Alg&#250;n Deporte</option>
            <option value="Verme Mejor Que Nunca" >Verme Mejor Que Nunca</option>
            </select>

            <div class=bmform_frmtext661886>
            &#191;Como te gustar&#237;a que nos contactemos? <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <select style='width:100%' name=fldfield22>
            <option value="Mail" >Mail</option>
            <option value="Whatsapp" >Whatsapp</option>
            <option value="Llamada Telef&#243;nica(muy recomendado)" >Llamada Telef&#243;nica(muy recomendado)</option>
            </select>
            </div>

            <div class=bmform_frmtext661886 style="text-align: left;">
            Mensaje <span style='color:#CC0000;font-size:125%;'> *</span> </div>
            <textarea style="height: 150px;" type=text class=bmform_frm661886 name=fldfield18 maxlength=100></textarea>

            <div class=bmform_button661886><input style="background-color: #DC2A2A;padding-top: 10px;padding-bottom: 10px;padding-left: 30px;padding-right: 30px;color: #fff;border: none;float: right;font-size: 20px;" type="submit" id="btnSubmit" value="Enviar"  krydebug="1751" class=bmform_submit661886 />
            </div></div>
            <div class=bmform_footer661886><div class=footer_bdy661886><div class=footer_txt661886></div></div></div>
            </div></div>
            </form></div>
          <!-- form -->
        </div>
    </div>
    <div class="triangulocontacto"></div>
    <div class="trianguloblog2"></div>
</section>


<?php get_footer(); ?>
