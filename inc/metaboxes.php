<?php
add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes()
{
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post'];
    $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
    if ($template_file == 'template-page/tpl_home.php') {
        add_meta_box('cyb-meta-textosec2', __('Descripción'), 'cyb_meta_sec2', 'page');
        add_meta_box('cyb-meta-textosec3', __('Programa'), 'cyb_meta_sec3', 'page');
        add_meta_box('cyb-meta-textosec4', __('Video'), 'cyb_meta_sec4', 'page');
        add_meta_box('cyb-meta-textosec5', __('Banner atención'), 'cyb_meta_sec5', 'page');
        add_meta_box('cyb-meta-faq', __('FAQ'), 'cyb_meta_faq', 'page');
        add_meta_box('cyb-meta-textosec6', __('Contacto'), 'cyb_meta_sec6', 'page');
    }
}

function cyb_meta_sec2(){
    global $post;
    $sec2 = get_post_meta($post->ID,'sec2',true);

    if(!empty($sec2)){
        $sec2_prod=$sec2;
    }
    else{
        $sec2_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<label>Texto top</label>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="textotop">'.$sec2_prod['textotop'].'</textarea><br><br>';
    $html .= "<input style='display:none;' id='image_location_sec2' type='text' name='wp_fancy_overlay_settings' value='".$sec2['imgtextbottom']."'>";
    $html .= '<input data-nameseccion="sec2" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagen" /><br><br>';
    $html .= '<div class="item_sec2"><div class="gal-img"><img src="'.json_decode($sec2['imgtextbottom'])[0]->thumbnail.'"><a class="remove_img">Borrar</a></div></div>';
    $html .= '<label>Texto bottom</label>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="textobottom">'.$sec2_prod['textobottom'].'</textarea>';
    $html .= '</div>';
    echo $html;
}


function cyb_meta_sec3(){
    global $post;
    $sec3 = get_post_meta($post->ID,'sec3',true);

    if(!empty($sec3)){
        $sec3_prod=$sec3;
    }
    else{
        $sec3_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<label>Banner</label><br><br>';
    $html .= "<input style='display:none;' id='image_location_sec3' type='text' name='imgbanner' value='".$sec3['imgbanner']."'>";
    $html .= '<input data-nameseccion="sec3" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagen" /><br><br>';
    $html .= '<div class="item_sec3"><div class="gal-img"><img src="'.json_decode($sec3['imgbanner'])[0]->thumbnail.'"><a class="remove_img">Borrar</a></div></div>';
    $html .= '<label>Plan nutricional</label>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="caja1">'.$sec3_prod['caja1'].'</textarea><br><br>';
    $html .= '<label>Rutina de ejercicios</label>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="caja2">'.$sec3_prod['caja2'].'</textarea>';
    $html .= '<label>Seguimientos</label>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="caja3">'.$sec3_prod['caja3'].'</textarea>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_sec4(){
    global $post;
    $sec4 = get_post_meta($post->ID,'sec4',true);

    if(!empty($sec4)){
        $sec4_prod=$sec4;
    }
    else{
        $sec4_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<label>Codigo del Video</label><br><br>';
    $html .= '<textarea style="width: 100%;" name="codigovideo">'.$sec4_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_sec5(){
    global $post;
    $sec5 = get_post_meta($post->ID,'sec5',true);

    if(!empty($sec5)){
        $sec5_prod=$sec5;
    }
    else{
        $sec5_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<label>Imagen</label><br><br>';
    $html .= "<input style='display:none;' id='image_location_sec5' type='text' name='imgbanneratencion' value='".$sec5_prod."'>";
    $html .= '<input data-nameseccion="sec5" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagen" /><br><br>';
    $html .= '<div class="item_sec5"><div class="gal-img"><img src="'.json_decode($sec5_prod)[0]->thumbnail.'"><a class="remove_img">Borrar</a></div></div>';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_faq(){
    global $post;
    $faq = get_post_meta($post->ID,'faq',true);

    if(!empty($faq)){
        $faq_prod=$faq;
    }
    else{
        $faq_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $settings = array('wpautop' => true, 'media_buttons' => false, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'faq' );

    $html = '<div>';
    $html .= wp_editor(wp_kses_post($faq_prod , ENT_QUOTES, 'UTF-8'), 'faqtext', $settings);
    $html .= '</div>';
    echo $html;
}

function cyb_meta_sec6(){
    global $post;
    $sec6 = get_post_meta($post->ID,'sec6',true);

    if(!empty($sec6)){
        $sec6_prod=$sec6;
    }
    else{
        $sec6_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= '<label>Datos de contacto</label><br>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="datoscontacto">'.$sec6_prod.'</textarea>';
    $html .= '</div>';
    echo $html;
}

add_action( 'save_post', 'dynamic_save_postdata' );
function dynamic_save_postdata( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['dynamicMeta_noncename2'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename2'], plugin_basename( __FILE__ ) ) )
        return;


    if( isset( $_POST['textotop']) || isset($_POST['textobottom'])){
        $datossec2=array('textotop' => $_POST['textotop'],'imgtextbottom'=>$_POST['wp_fancy_overlay_settings'],'textobottom'=>$_POST['textobottom']);
        update_post_meta($post_id,'sec2',$datossec2);
    }

    if( isset( $_POST['imgbanner']) || isset( $_POST['caja1']) || isset($_POST['caja2']) || isset($_POST['caja3'])){
        $datossec3=array('imgbanner' => $_POST['imgbanner'],'caja1' => $_POST['caja1'],'caja2'=>$_POST['caja2'],'caja3'=>$_POST['caja3']);
        update_post_meta($post_id,'sec3',$datossec3);
    }

    if( isset( $_POST['codigovideo'])){
        $datossec4=$_POST['codigovideo'];
        update_post_meta($post_id,'sec4',$datossec4);
    }

    if( isset( $_POST['imgbanneratencion'])){
        $datossec5=$_POST['imgbanneratencion'];
        update_post_meta($post_id,'sec5',$datossec5);
    }

    if( isset( $_POST['faq'])){
        $datosfaq=$_POST['faq'];
        update_post_meta($post_id,'faq',$datosfaq);
    }

    if( isset( $_POST['datoscontacto'])){
        $datossec6=$_POST['datoscontacto'];
        update_post_meta($post_id,'sec6',$datossec6);
    }
}